import { FETCH_PRODUCT_DETAILS, API, FETCH_PRODUCTS_DETAILS, OPEN_MODAL, OPEN_RECEIPT, ADD_PRODUCT_TO_RECEIPT, RESET_RECEIPT } from "./types"

function apiAction({
    url = "",
    method = "GET",
    data = null,
    onSuccess = () => {},
    onFailure = () => {},
    label = "",
    headersOverride = null
  }) {
    return {
      type: API,
      payload: {
        url,
        method,
        data,
        onSuccess,
        onFailure,
        label,
        headersOverride
      }
    };
  }

  
function setProductsDetails(data) {
    return {
      type: FETCH_PRODUCT_DETAILS,
      payload: data
    };
  }

  
export function fetchProductsDetails() {
  return apiAction({
    url: "https://my-json-server.typicode.com/marcopaoletti/db_junior_frontend/products", /* http://localhost:3000/mockup/mockup.json */
    onSuccess: setProductsDetails,
    onFailure: () => window.console.log("Error occured loading articles"),
    label: FETCH_PRODUCTS_DETAILS
  });
}

export const openModal = (productId) => {
	return {
		type: OPEN_MODAL,
		payload: productId
	}
}

export const openReceipt = () => {
  return {
    type: OPEN_RECEIPT,
    payload: {
      openReceipt: true
    }
  }
}

export const addProductToReceipt = (product) => {
  return {
    type: ADD_PRODUCT_TO_RECEIPT,
    payload: {
      product
    }
  }
}


export const resetReceipt = () => {
  return {
    type: RESET_RECEIPT
  }
}
