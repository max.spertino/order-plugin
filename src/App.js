import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { fetchProductsDetails } from "./actions" 
import Product from "./components/Product"
import Receipt from "./components/Receipt"
import './App.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from './components/Modal';


class App extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  componentDidMount() {
    const { fetchProductsDetailsProp } = this.props;
    fetchProductsDetailsProp();
  }
  
  render() {
    const { data = {} } = this.props;
    const { products = [] } = data;
    const { openReceipt } = this.props;
    const { productId, isLoadingData} = this.props;
    const product = products[productId];
    return (
      <div className="Main-container">
        <div className="Products-list">
          {isLoadingData   ? (
              "Loading . . ."
            ) : (
              products.map((product, key) => <Product key={product.name} id={key} product = {product} />) || ''
            )}     
        </div>
        {productId > -1 && <Modal product={product}/>
        }
        {openReceipt ? (
            <Receipt/>
          ) : (
            null
          )}
      </div>
     
    );
  }
}

App.propTypes = {
  openReceipt: PropTypes.bool.isRequired,
  data: PropTypes.shape({
    products: PropTypes.array
  }).isRequired,
  productId: PropTypes.number.isRequired,
  isLoadingData: PropTypes.bool.isRequired,
  fetchProductsDetailsProp: PropTypes.func.isRequired
}

const mapStateToProps = ({ data = {}, isLoadingData = false, productId = -1, openReceipt = false }) => ({
  data,
  isLoadingData,
  productId,
  openReceipt
});

export default connect(
  mapStateToProps,
  {
    fetchProductsDetailsProp: fetchProductsDetails
  }
)(App);
