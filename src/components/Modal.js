import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { openModal } from "../actions"
import './Modal.scss'



class Modal extends Component {
   
    constructor(props) {
        super();
        this.props = props;
    }

    closeModal = () => {
       
        const { openModalProp } = this.props
        openModalProp(-1);
    }

    render() {
        const { product } = this.props;
        const imgThumbStyle = product.img !== null ? {
            backgroundImage: `url(${product.img})`
        } : {
            backgroundColor: product.backgroundColor
        };

       
        return (
            <div className="Modal-bg">
                <div className="Modal">
                    <div className = "Img-thumb" style={imgThumbStyle}>
                            <div className="Overlay-text">
                                <span>{product.name}</span>
                            </div>
                            <div tabIndex="0" role = "button" className = "CloseBtn" onClick={this.closeModal} onKeyDown={this.closeModal}>
                                <span/>
                                <span/>
                            </div>
                    </div>
                    <div className="Title">
                        Ingredienti
                    </div>
                    <ul className="Ingredients">
                        {product.ingredients.map((ingredient) => <li key={`ing${ingredient}`}>{ingredient}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    product: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string.isRequired,
      backgroundColor: PropTypes.string.isRequired,
      img: PropTypes.string,
      ingredients: PropTypes.array
    }).isRequired,
    openModalProp: PropTypes.func.isRequired
}


export default connect(
    null,
    { openModalProp: openModal }
)(Modal)