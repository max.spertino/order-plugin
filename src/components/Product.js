import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from "react-redux"
import { openModal, openReceipt, addProductToReceipt } from "../actions"
import './Product.scss'


class Product extends Component {
  
  constructor(props) {
    super(props);
    this.props = props;
  }

  mouseDownCapture = (id) => {
    const openProductCart = () => {
      const { openModalProp } = this.props;
      openModalProp(id);
    }

    this.buttonPressTimer = setTimeout(
      () => {
        openProductCart();
      },
    500);
  }

  mouseUpCapture = () => {
    clearTimeout(this.buttonPressTimer);
  }

  addProductToReceipt = (product) => {
    

    const { openReceiptProp, addProductToReceiptProp } = this.props;
    openReceiptProp(true);
    addProductToReceiptProp(product);

  }

  render() {
    const { product, id } = this.props;

    const imgThumbStyle = product.img ? {
      backgroundImage: `url(${product.img})`
    } : {
      backgroundColor: product.backgroundColor
    };

    return (
      <div tabIndex="0" role = "button" className="Product" 
        onMouseDown={() => this.mouseDownCapture(id)} 
        onMouseUp={this.mouseUpCapture}  
        onMouseLeave={this.mouseUpCapture}
        onTouchStart={() => this.mouseDownCapture(id)} 
        onTouchEnd={this.mouseUpCapture}  
        onClick={() => this.addProductToReceipt({id, name: product.name, price: product.price})}
        onKeyDown={() => this.mouseDownCapture(id)}>
          <div className="Product-container">
            <div className = "Img-thumb" style={imgThumbStyle}>
            <div className="Overlay-text">
              <span>{product.name}</span>
            </div>
          </div>
          <div className="Product-price"> 
            &euro;{product.price}
          </div>
        </div>
      </div>
    )
  }
}


Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    img: PropTypes.string
  }).isRequired,
  openModalProp: PropTypes.func.isRequired,
  openReceiptProp: PropTypes.func.isRequired,
  addProductToReceiptProp: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired
}

export default connect(
  null,
  { 
    openModalProp: openModal,
    openReceiptProp: openReceipt,
    addProductToReceiptProp: addProductToReceipt
  }
)(Product)
