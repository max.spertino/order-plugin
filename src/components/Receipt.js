import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from "react-redux"
import { resetReceipt } from "../actions"
import './Receipt.scss'

class Receipt extends Component {
    constructor(props) {
        super(props);
        this.props = props;
    }

    resetReceiptHandle = () => {
        const { resetReceiptProp } = this.props;
        resetReceiptProp();
    }

    render() {
        const  { productsReceipt } = this.props;
        const productsReceiptArr = Object.values(productsReceipt);
        let totalPriceReceipt = 0; let totalQuantity = 0;
        
        productsReceiptArr.forEach((product) => {
            totalPriceReceipt += product.price*product.quantity;
            totalQuantity += product.quantity;
        })
        return (
            <div className="Receipt">
                <ul>
                {productsReceiptArr.map((product) =>
                    <li key={product.name}>
                        <span>{product.quantity} X {product.name}</span>
                        <span className="price">&euro; {product.price.toFixed(2).replace(".", ",")}</span>
                    </li>
                )}
                    <li className="Totals">
                        <div>
                            <span>PZ.</span>
                            <span>{totalQuantity}</span>
                        </div>
                        <div>
                            <span>TOT</span>
                            <span className="price">{totalPriceReceipt.toFixed(2).replace(".", ",")}</span>
                        </div>
                    </li>
                    <li className="Ctas">
                        <button type="button" onClick={this.resetReceiptHandle}>
                          TOTALE
                        </button>
                    </li>
                </ul>
            </div>
        );
    }
}



Receipt.propTypes = {
    productsReceipt: PropTypes.shape({
      name: PropTypes.string,
      price: PropTypes.number,
      quantity: PropTypes.number
    }).isRequired
}

Receipt.propTypes = {
    resetReceiptProp: PropTypes.func.isRequired
}

const mapStateToProps = ({ productsReceipt = [] }) => ({
    productsReceipt
});
  
export default connect(
    mapStateToProps,
    {
        resetReceiptProp: resetReceipt
    }
)(Receipt)