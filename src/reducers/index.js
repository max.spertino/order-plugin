import {
    FETCH_PRODUCT_DETAILS,
    API_START,
    API_END,
    FETCH_PRODUCTS_DETAILS,
    OPEN_MODAL,
    OPEN_RECEIPT,
    ADD_PRODUCT_TO_RECEIPT,
    RESET_RECEIPT
  } from "../actions/types";
  

const initialState = {
  state: {}
};

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PRODUCT_DETAILS:
      return { data: action.payload };
    case API_START:
      if (action.payload === FETCH_PRODUCTS_DETAILS) {
        return {
          ...state,
          isLoadingData: true
        };
      }
      break;
    case API_END:
      if (action.payload === FETCH_PRODUCTS_DETAILS) {
        return {
          ...state,
          isLoadingData: false
        };
      }
      break;
    case OPEN_MODAL:
      return {
        ...state,
        productId: action.payload
      };
    case OPEN_RECEIPT:
      return {
        ...state,
        openReceipt: action.payload.openReceipt
      };
    case ADD_PRODUCT_TO_RECEIPT: {
      const { product } = action.payload;
      let productsReceipt = { ...state.productsReceipt };
      if(!productsReceipt) productsReceipt = {};
      
      if(!productsReceipt[product.id]) {
          productsReceipt[product.id] = { ...product, quantity: 1 };
      } else {
          productsReceipt[product.id].quantity += 1;
      }
      return {
        ...state,
        productsReceipt
      };
    }
        
    case RESET_RECEIPT:
      return {
        ...state,
        productsReceipt: {}
      }; 
    default:
      return state;
  }

  return false;
}

export default rootReducer